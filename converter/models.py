import csv
import io
from xml.etree import ElementTree
from django.db import models
from django.core.files.base import ContentFile

from .utils import *


class XMLConverterModel(models.Model):
	xml = models.FileField(upload_to = 'xml')
	csv = models.FileField(upload_to = 'csv')

	class Meta:
		db_table = 'xml_to_csv'

	def save(self, *args, **kwargs):
		csv_content = self.convert_xml_to_csv(self.xml.file.read())
		csv_name = self.xml.name[:self.xml.name.rfind('.xml')] + '.csv'
		self.csv = ContentFile(csv_content, name = csv_name)
		super().save(*args, **kwargs)

	@staticmethod
	def convert_xml_to_csv(content):
		flatted_et = flat_etree(ElementTree.fromstring(content))
		with io.StringIO() as f:
			csv_dw = csv.DictWriter(f, fieldnames = flatted_et.keys())
			csv_dw.writeheader()
			csv_dw.writerow(flatted_et)
			converted_content = f.getvalue()
		return converted_content
