import re


def remove_namespace(tag):
    regex = re.compile(r'^{(.+)}')
    return regex.sub('', tag)


def flat_etree(et, prefix_tag=''):
    results = {}
    tag = f'{prefix_tag}/{remove_namespace(et.tag)}' if prefix_tag else remove_namespace(et.tag)
    results[tag] = et.text.strip() if et.text else ''
    for attr_key, attr_value in et.attrib.items():
        attr_tag = f'{tag}/@{attr_key}'
        results[attr_tag] = attr_value
    if not list(et):
        return results
    else:
        for child in list(et):
            results.update(flat_etree(child, tag))
        return results
