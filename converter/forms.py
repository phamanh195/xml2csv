from django import forms

from .models import XMLConverterModel


class XMLUploadForm(forms.ModelForm):
	class Meta:
		model = XMLConverterModel
		fields = ['xml']

	# def save(self, *args, **kwargs):
	# 	data = self.cleaned_data
	# 	file_content =
