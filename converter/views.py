from django.shortcuts import render, HttpResponse, redirect

from .forms import XMLUploadForm


def upload(request):
	if request.method == 'POST':
		form = XMLUploadForm(request.POST, request.FILES)
		if form.is_valid():
			obj = form.save()
			return HttpResponse(request.META['HTTP_ORIGIN'] + obj.csv.url)
	form = XMLUploadForm()
	return render(request, 'converter/upload.html',
	              {'form': form})
